#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

INSTALL := ./tools/install.sh

install: install-script install-service install-service-module install-service-instance-module install-configure install-rc-local
install-script: $(SCRIPT_TARGET:module/boot@/configure/%=$(DESTDIR)$(script_directory)/%)
install-service: $(SERVICE_TARGET:service/%=$(DESTDIR)$(service_directory)/%)
install-service-module: $(MODULE_TARGET:module/boot@/service/%=$(DESTDIR)$(module_directory)/boot@/service/%)
install-service-instance-module: $(MODULE_INSTANCE_TARGET:module/boot@/service@/%=$(DESTDIR)$(module_directory)/boot@/service@/%)
install-configure: $(MODULE_CONFIGURE_TARGET:module/boot@/configure/configure=$(DESTDIR)$(module_directory)/boot@/configure/configure)
install-rc-local: $(SKEL_SCRIPT_TARGET:module/boot@/configure/rc.local=$(DESTDIR)$(skel_directory)/rc.local)

$(DESTDIR)$(script_directory)/%: module/boot@/configure/%
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(module_directory)/boot@/configure/configure: module/boot@/configure/configure
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(skel_directory)/rc.local: module/boot@/configure/rc.local
	exec $(INSTALL) -D -m 755 $< $@

$(DESTDIR)$(module_directory)/boot@/service/%: module/boot@/service/%
	exec $(INSTALL) -D -m 644 $< $@

$(DESTDIR)$(module_directory)/boot@/service@/%: module/boot@/service@/%
	exec $(INSTALL) -D -m 644 $< $@

$(DESTDIR)$(service_directory)/%: service/%
	exec $(INSTALL) -D -m 644 $< $@
	sed -i -e "s,@adm_conf@,$(adm_conf)," \
		-e "s,@script_directory@,$(script_directory)," \
		-e "s,@skel_directory@,$(skel_directory)," \
		-e "s,@livedir@,$(livedir)," \
		-e "s,@VERSION@,$(version)," \
		-e "s,@HOSTNAME@,$(HOSTNAME)," \
		-e "s,@HARDWARECLOCK@,$(HARDWARECLOCK)," \
		-e "s,@TZ@,$(TZ)," \
		-e "s,@SETUPCONSOLE@,$(SETUPCONSOLE)," \
		-e "s,@TTY@,$(TTY)," \
		-e "s,@KEYMAP@,$(KEYMAP)," \
		-e "s,@FONT@,$(FONT)," \
		-e "s,@FONT_MAP@,$(FONT_MAP)," \
		-e "s,@FONT_UNIMAP@,$(FONT_UNIMAP)," \
		-e "s,@UDEV@,$(UDEV)," \
		-e "s,@SYSCTL@,$(SYSCTL)," \
		-e "s,@FORCECHCK@,$(FORCECHCK)," \
		-e "s,@LOCAL@,$(LOCAL)," \
		-e "s,@CONTAINER@,$(CONTAINER)," \
		-e "s,@TMPFILE@,$(TMPFILE)," \
		-e "s,@MODULE_KERNEL@,$(MODULE_KERNEL)," \
		-e "s,@MODULE_SYSTEM@,$(MODULE_SYSTEM)," \
		-e "s,@RANDOMSEED@,$(RANDOMSEED)," \
		-e "s,@FSTAB@,$(FSTAB)," \
		-e "s,@SWAP@,$(SWAP)," \
		-e "s,@LVM@,$(LVM)," \
		-e "s,@DMRAID@,$(DMRAID)," \
		-e "s,@BTRFS@,$(BTRFS)," \
		-e "s,@ZFS@,$(ZFS)," \
		-e "s,@ZFS_IMPORT@,$(ZFS_IMPORT)," \
		-e "s,@CRYPTTAB@,$(CRYPTTAB)," \
		-e "s,@FIREWALL@,$(FIREWALL)," \
		-e "s,@CGROUPS@,$(CGROUPS)," \
		-e "s,@MNT_PROC@,$(MNT_PROC)," \
		-e "s,@MNT_SYS@,$(MNT_SYS)," \
		-e "s,@MNT_DEV@,$(MNT_DEV)," \
		-e "s,@MNT_RUN@,$(MNT_RUN)," \
		-e "s,@MNT_TMP@,$(MNT_TMP)," \
		-e "s,@MNT_PTS@,$(MNT_PTS)," \
		-e "s,@MNT_SHM@,$(MNT_SHM)," \
		-e "s,@MNT_NETFS@,$(MNT_NETFS)," \
		-e "s,@POPULATE_SYS@,$(POPULATE_SYS)," \
		-e "s,@POPULATE_DEV@,$(POPULATE_DEV)," \
		-e "s,@POPULATE_RUN@,$(POPULATE_RUN)," \
		-e "s,@POPULATE_TMP@,$(POPULATE_TMP)," $@

version:
	@echo $(version)
	
.PHONY: install version

.DELETE_ON_ERROR:
