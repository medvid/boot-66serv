# Changelog for boot-66serv

---

# In 2.1.0

- Adapt to oblibs 0.0.9.0, 66 0.4.0.0 and 66-tools 0.0.6.0
- Bugs fix:
	- ZFS_IMPORT valid value
	- regex of the LIVE variable at all-Runtime in case of crash
	- retrieve all behavior of the system-random service.

- *all-Runtime*	services do not crash anymore if a runtime service fails to start. It only warns the user about the state.
- Symlink previously at system_administration/ (e.g. /etc/66) is no longer created.
- *Udevd* has its own logger created at the livedir/log/udevd (e.g. /run/66/log/udevd).
- *configure* script now respects the choice about the color set at *66-enable*.
- According to the 66 >v0.4.0.0, all @build fields were removed. These fields are no longer mandatory.
- Reorganize the environment configuration file to be clearer as much as possible for lambda user.

---

# In 2.0.0
	
- Pass to module type.
- A lot of flags were added. Please see `configure --help` for further details which are self-explanatory.

---

# In 1.2.1
	
- Bugs and typo fix

---
	
# In 1.2.0

- Bugs fix: tmpfiles.sh
- Modules.sh is now pure sh
- Fix swap file: active swap after remounting / rw
- Full support of encryption password using 66-olexec

---

# In 1.1.2
	
- Bugs fix: fix devices-zfs makefile variable

---
	
# In 1.1.1
	
- Pass zfs to system-Devices bundle
- Wait for all-Mount at system-fsck
- Mount cgroups before run udevd

---
	
# In 1.1.0
	
- Fix mount of cgroups and zfs
- Update tmpfiles.sh script
- Add local-sethostname service: it overwrite /etc/hostname file with the value of HOSTNAME from boot.conf
- Ported to 66 version 0.2.1.0: remove @name field

---

# In 1.0.1
	
- Fix permissions of /tmp/.X11-unix and /tmp/.ICE-unix to 1777

---
	
# In 1.0.0

- first commit.
