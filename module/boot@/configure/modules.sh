#!@BINDIR@/sh
#
# This scripts is a pure-POSIX sh version
#
# Copyright (c) 2015-2020 Eric Vidal <eric@obarun.org>
# All rights reserved.
# 
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.


# Configuration files are read from directories in
# /usr/lib/modules-load.d, /run/modules-load.d, and /etc/modules-load.d,
# in order of precedence

MODULES_PATH='/etc/modules-load.d /run/modules-load.d /usr/lib/modules-load.d'
MODULES_NAME=""
MODULES_RESULT=""

check_elements(){
	for e in $2; do [ "$e" = "$1" ] && return 0; done; return 1;
}

build_array() {
	printf "%s %s" "$1" "$2" 
}

check_file(){
	
	tidy_loop="" conf=""
	
	for tidy_loop in $MODULES_PATH; do
		if [ -d "${tidy_loop}" ]; then
			for conf in "${tidy_loop}"/*.conf ; do
				if [ -f "${conf}" ]; then
					if ! check_elements "${conf##*/}" "${MODULES_NAME}"; then
						MODULES_NAME=$(build_array "${MODULES_NAME}" "${conf##*/}")
					fi
				fi
			done
		fi
	done
	
	unset tidy_loop conf
}
check_path(){
	path="" tidy_loop=""
	for path in ${MODULES_PATH}; do
		for tidy_loop in ${MODULES_NAME}; do
			if [ -f "${path}/${tidy_loop}" ]; then
				if ! check_elements "${tidy_loop}" "${MODULES_RESULT##*/}";then
					MODULES_RESULT=$(build_array "${MODULES_RESULT}" "${path}/${tidy_loop}")
				fi
			fi
		done
	done
	unset path tidy_loop
}

check_file
if [ -n "${MODULES_NAME}" ]; then
	check_path
else
	printf "$0: %s\n" "No modules found -- nothing to do"
	exit 0
fi
for mod in ${MODULES_RESULT}; do
	while read -r line; do
		comment=$(printf "%s" "$line" | awk  '{ string=substr($0, 0, 1); print string; }' )
		if [ "${comment}" = "#" ] || [ -z "${line}" ];then
			continue
		fi
		for check in ${line};do
			modprobe -b "${check}" -v | sed 's:insmod [^ ]*/:Load modules :g; s:\.ko\(\.gz\)\? ::g'
		done
	done < "${mod}"
done

exit 0
